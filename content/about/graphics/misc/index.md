---
title: "Miscellaneous"
weight: 300
---
This page contains miscellaneous other graphics projects.

### Maturitní ples
I put together the graphical idenitity of our school prom. I worked with another
girl who made the concept and the drawings while I turned those into a proper
graphical presentation, with a custom modification of a font included.

{{< blogPhoto src="maturak.jpg" alt="A graphic which was shown on the LED screen at the venue and the poster." >}}

### Open Day Online of our school
During COVID, our school released a [series of videos and a
livestream](https://gbl.cz/dod2021/) which were supposed to replace the
traditional Open Day for new applicants. I worked on the technical side of
things and helped bring a graphical concept into reality.

{{< blogPhoto src="dod-web.png" alt="The website I made for easy access to the recorded videos." >}}
