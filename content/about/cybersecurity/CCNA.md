---
title: "CCNA"
weight: 400
---

I did the CCNA CyberOps course as a part of an internship in 2020. To be perfectly honest I didn't find it very useful, it was filled with buzzwords and weird diagrams that made very little sense, but I have to admit there might have been a word or two of useful stuff. So if you (unlike me) like these certificates, there you go, I've got one.



