---
title: "Pátek"
weight: 350
image:
  src: "patek-promo.jpg"
  alt: "Pátek's promotional graphic"
---

I am a founding member of [Pátek](https://patekvpatek.cz/en), which is a club/makerspace in my school. We're mainly CompSci nerds, but we like to play around with all kinds of STEM. Most of our time is spent talking, sharing knowledge and experiences and helping each other with our own personal projects. We also take care of the school's 3D printers, sometimes organize talks. Pátek is the place where my love for Linux and programming began. 

{{< blogPhoto src="patek-promo.jpg" alt="Pátek's promotional graphic" caption=false >}}