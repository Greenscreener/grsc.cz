---
title: "Miscellaneous"
weight: 400
---

Some other cool things that I've made but that aren't interesting enough to get their own section.

### TwitterStickerBot

A Telegram bot which takes images from a Twitter account and turns them into a sticker pack.

[Source](https://github.com/Greenscreener/twitter-to-telegram-stickers-bot)

### Covid19 district graph

A simple web app which fetches Czech republic's open data and displays a graph of relative incidence for each district. 

[Link](https://stuff.grsc.cz/covid19-okresy-graf/), [Source](https://github.com/Greenscreener/stuff)

### Bash.js

A very old project that I did mostly to learn JavaScript. It is a simulated command line that doesn't have any real use besides looking kinda cool. The most interesting part is probably a filesystem-like structure, that allows basic interaction with files in the command line.

[Link](http://bashjs.greenscreener.tk/), [Source](https://github.com/Greenscreener/bash.js)



