---
title: "PátekVPátek.cz"
weight: 200
alias: /about/making/web/patek-cz/
image:
  src: "patek-cz.png"
  alt: "A screenshot of Pátek.cz."
---

I designed the [official website](https://patekvpatek.cz)  of [Pátek]({{< relref "/about/making/patek" >}}) and even though it was at first mostly my code, many people contributed to it since. It also uses the same technology as our [school's website]({{< relref "/about/making/web/gbl-cz" >}}), so we use it as a testing ground for new features, for instance CI/CD. Unlike the school's website, PátekVPátek.cz is [Open Source](https://gitlab.com/patek-devs/patek.cz). The site is translated to English, but we currently don't translate any of the content that's published regularly.

{{< blogPhoto src="patek-cz.png" alt="A screenshot of PátekVPátek.cz." format="png" >}}
