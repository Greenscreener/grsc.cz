---
title: "GBL.cz"
weight: 300
image:
  src: "gbl-cz.png"
  alt: "A screenshot of gbl.cz."
---

In the same way [we got in charge of our school's network]({{< relref "/about/sysadmin/school" >}}), we started taking care of the school's website a few years prior. I worked mostly on the design of the site and a JS backend for authenticating and fetching data from our school IS. There were some design decisions that I would probably make differently now, but developing the website was a great learning experience for all of us and I think it ended up quite nice. Publishing the source is currently on our roadmap, but there might or might not be some burried credentials in the git repo, so we need to clean it up a bit first. 

{{< blogPhoto src="gbl-cz.png" alt="A screenshot of gbl.cz." format="png" >}}
