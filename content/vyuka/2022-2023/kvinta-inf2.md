---
title: "Informatika Kvinta 2022/2023 Skupina 2"
date: 2022-09-04T00:03:00+02:00
draft: false
tags: []
---

# Informatika Kvinta 2022/2023 Skupina 2

## pá 9:55 – 11:35, učebna 117

[kurz v Google Classroom](https://classroom.google.com/c/NTI3MDY2MzA5NzY1)

## Témata

První pololetí:

- reprezentace dat 					
	- celá čísla s a bez znaménka, racionální čísla
	- text
	- CSV
	- obrázky
	- zvuk
- jednotky velikosti dat
- databáze
- principy fungování počítačů

## Pravidla hry

### Bodování

Na každé vyučovací hodině dostanete zadaný úkol, na kterém budete pracovat v hodině, co nestihnete, doděláte doma. Za správné splnění úkolu dle zadání dostanete obvykle **10 bodů**, můžete ale dostat **bonusové body** za řešení nad rámec úkolu. Může to být bonusová úloha popsaná přímo v zadání úkolu, ale fantasii se meze nekladou, pokud například řešení nějak rozšíříte nebo vyřešíte pomocí nově nabytých znalostí nějaký zcela nesouvisející problém, rád se o tom dozvím a přihodím nějaký ten bod. 

### Pozdní odevzdání

Za pozdní odevzdání si vyhrazuji odečítat až **3 body** (nikdy pod nulu), ale pokud se mi včas ozvete, že nestíháte, a brzy poté dodáte i úkol samotný, snížím počet odečtených bodů, a to i na **nulu**. Ale pozor, pozdě dodané úkoly nemusím stihnout včas opravit.

### Podmínky klasifikace

Abych vás klasifikoval, musíte získat z úkolů zadaných v každém kalendářním měsíci alespoň **8 bodů**. 

### Výpočet známky

Podle maximálního počtu bodů a procent ve školním řádu se ze získaných bodů vypočítá známka. Do tohoto "maxima" se ale nepočítají bonusové body, můžete proto dostat bodů více a mít tak "více než 100 %".

#### Známka za první pololetí

| známka | procentní rozsah | bodový rozsah |
| ------ | ---------------- | ------------- |
| 1      | ≥85%             | ≥ 108         |
| 2      | 84-69%           | 107-88        |
| 3      | 68-52%           | 87-67         |
| 4      | 51-33%           | 66-42         |
| 5      | ≤32%             | ≤41           |

### Odevzdávání

Prosím, odevzdávejte úkoly **pouze na Classroom**, a to odevzdáním všech souborů k příslušnému zadání. Také prosím, pokud možno, využívejte soukromých komentářů, pokud očekáváte moji rychlou odpověď -- tedy pokud máte nějaký dotaz, nevíte si rady nebo si nejste něčím jisti. Naopak samotné řešení úkolu, odpovědi na otázky a zdůvodnění vkládejte prosím v libovolném formátu jako přílohy v sekci *Vaše práce*. 

Tato odevzdávací pravidla, nebo spíše prosby, nemusíte dodržovat, ale velmi mi tím ulehčíte práci a urychlíte mé odpovědi vašim kolegům. Samozřejmě mi však záleží hlavně na tom, abyste si z úkolů něco vzali a pokud by vám tato forma odevzdání dočasně či trvale nevyhovovala, stačí, když se ke mně odpovědi *nějak dostanou*.

### To nejdůležitější na konec

Jak jsem zmínil, je hlavní, abyste si z hodin a úkolů něco odnesli. Pokud vám něco není jasné, nejspíš to znamená, že jsem to špatně vysvětlil, proto, ptejte se, *ptejte se*, **ptejte se** -- toto platí během hodin, ale i mimo ně, nebojte se mě kontaktovat na [mailu](mailto:j.cernohorsky@gbl.cz), Classroomu nebo jakkoliv jinak dovedete (klidně můžete zkusit křičet z okna, ale negarantuji, že vás uslyším). To samé platí i když si nevíte rady s úkolem.

Úkoly **lze** odevzdávat vícekrát -- pokaždé, když vám úkol vrátím, ohodnotím ho a pokud nebude podle mých představ, pokusím se vás posunout správným směrem. **Využijte toho** -- pokud si s něčím nejste jisti, klidně úkol odevzdejte, vždycky budete mít další pokus. 
