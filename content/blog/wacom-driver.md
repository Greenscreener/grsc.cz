---
title: "My first pull request: Smooth scrolling for the Wacom Linux driver"
date: 2022-12-14T16:15:52+01:00
tags: [ "en" ]
---
A few weeks ago my first ever pull request to a larger open-source project got
merged, but it was quite a long journey, so let's start from the beginning.

## The issue
Some time ago, I got myself a Wacom drawing tablet. It worked out of the box,
which I found delightful, but one thing started to bug me quite quickly. The
driver had a *panscrolling* feature -- when holding a button, dragging the pen
across the tablet scrolled instead of drawing. However, this was done in a
peculiar way -- instead of the screen moving smoothly as you moved your pen, it
jumped around as if it was a huge scrollwheel.

I opened an issue and suggested I'll try fixing it by myself. At that point, I
had no idea what I was getting myself into, but the maintainer, **@whot** was
very helpful and pointed me in the right direction.

Turns out that when this feature was implemented, X11 did not even support
smooth scrolling and sending scroll wheel button events was the only way to do
things and nobody just had the energy to reimplement it since then.

## The pull request
To cut a very long story short, I worked on the project intermittently for
quite a while and while doing so, managed to skim through most of the source
code of the driver, getting a general undestanding of how it works. I even
managed to find and sometimes even fix some bugs and mistakes that got
introduced into the code in the past. When I started, I thought it was going to
be just a quick modification to a single function, but it ended up needing to
sift through most of the driver and make sure nothing broke. It was also a great
git exercise, because keeping a pull request running alongside an active project
involves resolving the many conflicts that inevitably arise.

Nevertheless, after almost ten months, the 
[request got finally merged](https://github.com/linuxwacom/xf86-input-wacom/pull/222) 
and the changes will hopefully get shipped in the coming months.
