---
title: "µpost: Pound signs and terminal control sequences"
date: 2021-12-26T19:00:36+01:00
tags: [ "en", "upost" ]
---
This afternoon I was XORing some data together, as you do, and I noticed there were pound symbols among the random characters. 

{{< blogPhoto src="pound-signs.png" alt="The random terminal output with pound signs." format="png" >}}

This wouldn't be normally so odd, but I knew that my data *should* contain only ASCII characters and XORing ASCII characters together should only produce more ASCII characters, right?

<details>

<summary>A detailed description of me losing my mind</summary>

I first started checking my input to see if it really was pure ASCII. I tried to look for no-break-spaces and other fancy whitespace, but to no avail. Then I looked through my code, what if converting from characters to signed integers caused some weird inconsistencies when XORing? Nope, not that either. I tried printing out all the different unique char codes that were in the output and surely enough, all of them were 0-127. At this point I was desperate and thought to myself: "What if the pound sign actually *is* in ASCII and I've just never noticed it?" I copied the symbol over into a python shell with `ord("£")` and sure enough, I got `163` out, which is clearly larger than 127, so, obviously out of range of ASCII. 

{{< blogPhoto src="wat.png" alt="A giant inflatable rubber duck." caption="The duck from https://www.destroyallsoftware.com/talks/wat" >}}

</details>

<br>

Well, turns out that, because the "terminals" or terminal emulators of today use essentially the same protocol as actual [terminals](https://en.wikipedia.org/wiki/Computer_terminal), there are some left overs that don't have much use these days. There are escape codes for things raging from colour, which most of us use, through [blinking text](blinking.mp4), to [National Replacement Character Sets](https://en.wikipedia.org/wiki/National_Replacement_Character_Set) -- escape codes, which make your terminal replace certain ASCII characters with different ones, for instance the hash sign (#) with the pound sign (£).

The escape sequence in particular is `ESC ( A` and sure enough:

<figure><video controls src="escape%20code.mp4"></video><figcaption> The escape sequence being used in the terminal.</figcaption></figure>



