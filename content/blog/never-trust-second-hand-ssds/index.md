---
title: "Never trust second-hand SSDs"
date: 2024-08-27T16:50:36+02:00
tags: [ "en", "upost" ]
---
I like to buy second-hand electronics, both as a way to save some money, to
maybe help reduce e-waste a bit, and I personally find the absence of any
warranty quite liberating, because I know I can do whatever I please to my
devices (like, idk, [drilling into them]({{< relref "/about/making/hw/#usb-c-ifying-my-x230" >}})).
For the most part, I've had a pleasant experience doing so.

There's one thing, that I've always knew I should *never* get second-hand –
SSDs. Whenever I bought a new computer, I'd replace the main drive with a fresh
and new SSD. And for good reason: SSDs, especially cheap ones, have a very
limited life-span, and if you don't know what the SSD has been through, it is
usually safe to assume it is not that far from death.

Yeah, so I bought this wonderful ThinkPad Yoga L380 second hand recently and
forgot to replace the SSD. Basically throughout the time I had the computer, it
experienced random few-second freezes. During the summer, it kernel panicked on
me a few times and last week, it woke up from sleep without a boot drive a few
times. As I was somewhat busy, I ignored the issues mostly, figuring it would be
*some driver issue* and that it ought to be fixed by an update at some point.

Yesterday, the issues started getting exponentially worse and when I realised
what was wrong and started attempting to save at least my SSH keys, the SSD
stopped reporting to the SATA port at all.

So yeah. **Never trust second-hand SSDs.**

{{< blogPhoto src="bad ssd.jpg" alt="Image of the broken SSD beside the disassembled laptop with a hand flipping the SSD off" caption=false >}}

{{< blogPhoto src="matůš.jpg" alt="My computer showing this post with mutiple people flipping the SSD off" caption=false >}}
